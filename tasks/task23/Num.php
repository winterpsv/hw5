<?php

declare(strict_types=1);

namespace tasks\task23;

/**
 * Class Num
 * @package tasks\task23
 */
class Num
{
    public const NUM5 = 7;
    private const NUM6 = 13;

    /** @var int $num1 */
    public static int $num1;

    /** @var int $num2 */
    public static int $num2;

    /** @var int $num3 */
    private static int $num3 = 3;

    /** @var int $num4 */
    private static int $num4 = 5;

    /**
     * @return int
     */
    public static function getSum(): int
    {
        return self::$num3 + self::$num1;
    }

    /**
     * @return int
     */
    public static function getNum6(): int
    {
        return self::NUM6;
    }
}
