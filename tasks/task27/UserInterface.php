<?php

declare(strict_types=1);

namespace tasks\task27;

/**
 * Interface UserInterface
 * @package tasks\task27
 */
interface UserInterface
{
    public function setName($name);
    public function getName();
    public function setAge($age);
    public function getAge();
}
