<?php

declare(strict_types=1);

namespace tasks\task27;

/**
 * Class Employee
 * @package tasks\task27
 */
class Employee implements EmployeeInterface
{
    /** @var string $name */
    private string $name;

    /** @var int $age */
    private int $age;

    /** @var int $salary */
    private int $salary;

    /**
     * @param $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param $salary
     */
    public function setSalary($salary): void
    {
        $this->salary = $salary;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }
}
