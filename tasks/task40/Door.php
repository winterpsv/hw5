<?php

declare(strict_types=1);

namespace tasks\task40;

/**
 * Class Door
 * @package tasks\task40
 */
class Door implements Specifications
{
    /** @var string $color */
    private string $color;

    /** @var string $type */
    private string $type;

    /**
     * Door constructor.
     * @param $color
     * @param $type
     */
    public function __construct($color, $type)
    {
        $this->color = $color;
        $this->type = $type;
    }

    public function open()
    {
        return 'door opened';
    }

    public function close()
    {
        return 'door closed';
    }
}
