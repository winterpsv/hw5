<?php

declare(strict_types=1);

namespace tasks\task40;

/**
 * Class Window
 * @package tasks\task40
 */
class Window implements Specifications
{
    /** @var array $glazing */
    private array $glazing;

    /** @var array $size */
    private array $size;

    /**
     * Door constructor.
     * @param $glazing
     * @param $size
     */
    public function __construct($glazing, $size)
    {
        $this->glazing = $glazing;
        $this->size = $size;
    }

    public function open()
    {
        return 'window opened';
    }

    public function close()
    {
        return 'window closed';
    }
}
