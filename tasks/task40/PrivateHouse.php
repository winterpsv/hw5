<?php

declare(strict_types=1);

namespace tasks\task40;

/**
 * Class PrivateHouse
 * @package tasks\task40
 */
class PrivateHouse extends Building
{
    //some unique functionality
}
