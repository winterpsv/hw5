<?php

declare(strict_types=1);

namespace tasks\task40;

use tasks\task40\Window as Window40;
use tasks\task40\Door as Door40;

/**
 * Class Building
 * @package tasks\task40
 */
class Building
{
    /** @var string $name */
    private string $name;

    /** @var array $specifications */
    private array $specifications = [];

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param Specifications $specifications
     */
    public function addSpecifications(Specifications $specifications)
    {
        $this->specifications[] = $specifications;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $status
     * @return mixed
     */
    public function getDoor(string $status)
    {
        foreach ($this->specifications as $spec) {
            if($spec instanceof Door40) {
                return $spec->$status();
            }
        }
    }

    /**
     * @param string $status
     * @return mixed
     */
    public function getWindow(string $status)
    {
        foreach ($this->specifications as $spec) {
            if($spec instanceof Window40) {
                return $spec->$status();
            }
        }
    }
}
