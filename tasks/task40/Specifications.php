<?php

declare(strict_types=1);

namespace tasks\task40;

/**
 * Interface Specifications
 * @package tasks\task40
 */
interface Specifications
{
    public function open();
    public function close();
    //other functionality
}
