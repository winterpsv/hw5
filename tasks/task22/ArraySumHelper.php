<?php

declare(strict_types=1);

namespace tasks\task22;

/**
 * Class ArraySumHelper
 * @package tasks\task22
 */
class ArraySumHelper
{
    /** @var int $num */
    public static int $num;

    /**
     * @param array $arr
     * @param int $num
     *
     * @return array
     */
    public static function arrayElementMultiplier(array $arr, int $num): array
    {
        self::$num = $num;
        return array_map(function ($item){
            return $item * self::$num;
        }, $arr);
    }
}
