<?php

declare(strict_types=1);

namespace tasks\task28;

/**
 * Class Rectangle
 * @package tasks\task28
 */
final class Rectangle implements Figure
{
    /** @var int $sideA */
    private int $sideA;

    /** @var int $sideB */
    private int $sideB;

    /**
     * Rectangle constructor.
     * @param $sideA
     * @param $sideB
     */
    public function __construct($sideA, $sideB)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
    }

    /**
     * @return int
     */
    public function getSquare(): int
    {
        return $this->sideA * $this->sideB;
    }

    /**
     * @return int
     */
    public function getPerimeter(): int
    {
        return 2 * ($this->sideA + $this->sideB);
    }
}
