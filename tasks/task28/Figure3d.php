<?php

declare(strict_types=1);

namespace tasks\task28;

/**
 * Interface Figure3d
 * @package tasks\task28
 */
interface Figure3d
{
    public function getVolume();
    public function getSurfaceSquare();
}
