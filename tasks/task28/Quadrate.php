<?php

declare(strict_types=1);

namespace tasks\task28;

/**
 * Class Quadrate
 * @package tasks\task28
 */
final class Quadrate implements Figure
{
    /** @var int $side */
    private int $side;

    /**
     * Quadrate constructor.
     * @param $side
     */
    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return int
     */
    public function getSquare(): int
    {
        return pow($this->side, 2);
    }

    /**
     * @return int
     */
    public function getPerimeter(): int
    {
        return 4 * $this->side;
    }
}
