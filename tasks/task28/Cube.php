<?php

declare(strict_types=1);

namespace tasks\task28;

/**
 * Class Cube
 * @package tasks\task28
 */
final class Cube implements Figure3d
{
    /** @var int $side */
    private int $side;

    /**
     * Cube constructor.
     * @param $side
     */
    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return int
     */
    public function getVolume(): int
    {
        return pow($this->side, 3);
    }

    /**
     * @return int
     */
    public function getSurfaceSquare(): int
    {
        return 6 * pow($this->side, 2);
    }
}
