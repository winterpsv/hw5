<?php

declare(strict_types=1);

namespace tasks\task28;

/**
 * Interface Figure
 * @package tasks\task28
 */
interface Figure
{
    public function getSquare();
    public function getPerimeter();
}
