<?php

declare(strict_types=1);

namespace tasks\task8;

/**
 * Class Student
 * @package tasks\task8
 */
class Student
{
    protected const PATTERN = '/^[a-zA-Zа-яА-Я\s\-]+$/u';

    /** @var string $name */
    public string $name;

    /** @var string $surname */
    public string $surname;

    /** @var int $course 1 - 5 */
    public int $course = 1;

    /** @var string $courseAdministrator */
    private string $courseAdministrator;

    /** @var string $courseAdministratorSurname */
    private string $courseAdministratorSurname;

    /**
     * Set course +1 if course<5
     */
    public function transferToNextCourse(): void
    {
        (!$this->isCourseCorrect()) ?: $this->course = $this->course + 1;
    }

    /**
     * @param string $name
     */
    public function setCourseAdministrator(string $name): void
    {
        $this->courseAdministrator = $name;
    }

    /**
     * @return string
     */
    public function getCourseAdministrator(): string
    {
        return $this->courseAdministrator;
    }

    /**
     * @return string
     */
    public function getCourseAdministratorSurname(): string
    {
        return $this->courseAdministratorSurname;
    }

    /**
     * @param string $courseAdministratorSurname
     */
    public function setCourseAdministratorSurname(string $courseAdministratorSurname): void
    {
        $this->courseAdministratorSurname = $courseAdministratorSurname;
    }

    /**
     * @return bool
     */
    protected function validateCourseAdministrator(): bool
    {
        if (!preg_match(self::PATTERN, $this->courseAdministrator)) {
            return false;
        }

        if (!preg_match(self::PATTERN, $this->courseAdministratorSurname)) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function isCourseCorrect(): bool
    {
        return $this->course < 5;
    }
}
