<?php

declare(strict_types=1);

namespace tasks\task3;

/**
 * Class User
 * @package tasks\task3
 */
class User
{
    /** @var string $name */
    public string $name;

    /** @var int $age */
    public int $age;

    /** @var int $salary */
    protected int $salary;

    /**
     * @param int $newAge
     */
    public function setAge(int $newAge): void
    {
        ($newAge < 18) ?: $this->age = $newAge;
    }

    /**
     * @param int $salary
     */
    protected function setSalary(int $salary): void
    {
        $this->salary = $salary;
    }

    /**
     * @param int $salary
     *
     * @return bool
     */
    protected function validateSalary(int $salary): bool
    {
        return $salary > 500;
    }
}
