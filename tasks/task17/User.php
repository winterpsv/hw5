<?php

declare(strict_types=1);

namespace tasks\task17;

/**
 * Class User
 * @package tasks\task17
 */
class User
{
    /** @var string $name */
    public string $name;

    /** @var string $surname */
    public string $surname;
}
