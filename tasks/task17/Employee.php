<?php

declare(strict_types=1);

namespace tasks\task17;

/**
 * Class Employee
 * @package tasks\task17
 */
class Employee extends User
{
    /** @var int $salary */
    public int $salary;
}
