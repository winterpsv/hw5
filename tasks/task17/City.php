<?php

declare(strict_types=1);

namespace tasks\task17;

/**
 * Class City
 * @package tasks\task17
 */
class City
{
    /** @var string $name */
    public string $name;

    /** @var int $population */
    public int $population;
}
