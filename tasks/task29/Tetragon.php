<?php

declare(strict_types=1);

namespace tasks\task29;

/**
 * Interface Tetragon
 * @package tasks\task29
 */
interface Tetragon
{
    public function getSideA();
    public function getSideB();
    public function getSideC();
    public function getSideD();
}
