<?php

declare(strict_types=1);

namespace tasks\task29;

/**
 * Class Disk
 * @package tasks\task29
 */
final class Disk implements Circle
{
    /** @var int $rad */
    private int $rad;

    /**
     * Disk constructor.
     * @param $rad
     */
    public function __construct($rad)
    {
        $this->rad = $rad;
    }

    /**
     * @return int
     */
    public function getRadius()
    {
        return $this->rad;
    }

    /**
     * @return float|int
     */
    public function getDiameter()
    {
        return 2 * $this->rad;
    }

    /**
     * @return float|int
     */
    public function getSquare()
    {
        return Circle::P * pow($this->rad, 2);
    }

    /**
     * @return float|int
     */
    public function getPerimeter()
    {
        return 2 * Circle::P * $this->rad;
    }

}