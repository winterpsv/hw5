<?php

declare(strict_types=1);

namespace tasks\task29;

use tasks\task28\Figure as Figure28;

/**
 * Class Rectangle
 * @package tasks\task29
 */
final class Rectangle implements Figure28, Tetragon
{
    /** @var int $sideA */
    private int $sideA;

    /** @var int $sideB */
    private int $sideB;

    /** @var int $sideC */
    private int $sideC;

    /** @var int $sideD */
    private int $sideD;

    /**
     * Rectangle constructor.
     * @param $sideA
     * @param $sideB
     * @param $sideC
     * @param $sideD
     */
    public function __construct($sideA, $sideB, $sideC, $sideD)
    {
        $this->sideA = $sideA;
        $this->sideB = $sideB;
        $this->sideC = $sideC;
        $this->sideD = $sideD;
    }

    /**
     * @return float|int
     */
    public function getSquare()
    {
        return $this->sideA * $this->sideB;
    }

    /**
     * @return int
     */
    public function getPerimeter()
    {
        return $this->sideA + $this->sideB + $this->sideC + $this->sideD;
    }

    /**
     * @return int
     */
    public function getSideA(): int
    {
        return $this->sideA;
    }

    /**
     * @return int
     */
    public function getSideB(): int
    {
        return $this->sideB;
    }

    /**
     * @return int
     */
    public function getSideC(): int
    {
        return $this->sideC;
    }

    /**
     * @return int
     */
    public function getSideD(): int
    {
        return $this->sideD;
    }
}
