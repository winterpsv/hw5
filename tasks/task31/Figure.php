<?php

declare(strict_types=1);

namespace tasks\task31;

/**
 * Interface Figure
 * @package tasks\task31
 */
interface Figure
{
    public function getSquare();
    public function getPerimeter();
    public function getSumSP();
}
