<?php

declare(strict_types=1);

namespace tasks\task31;

/**
 * Class FiguresCollection
 * @package tasks\task31
 */
final class FiguresCollection
{
    /** @var array $collection */
    private array $collection = [];

    public function addFigure(Figure $figure)
    {
        $this->collection[] = $figure;
    }

    public function getTotalSquare()
    {
        $TotalSquare = 0;
        foreach ($this->collection as $figure) {
            $TotalSquare = $TotalSquare + $figure->getSquare();
        }
        return $TotalSquare;
    }
}
