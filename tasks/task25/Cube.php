<?php

declare(strict_types=1);

namespace tasks\task25;

/**
 * Class Cube
 * @package tasks\task25
 */
class Cube implements CubeInterface
{
    /** @var int $side */
    private int $side;

    /**
     * Cube constructor.
     * @param $side
     */
    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return int
     */
    public function getVolumeCube(): int
    {
        return pow($this->side, 3);
    }

    /**
     * @return int
     */
    public function getSurfaceArea(): int
    {
        return 6 * pow($this->side, 2);
    }
}
