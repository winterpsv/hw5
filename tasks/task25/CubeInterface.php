<?php

declare(strict_types=1);

namespace tasks\task25;

/**
 * Interface CubeInterface
 * @package tasks\task25
 */
interface CubeInterface
{
    public function __construct($side);
    public function getVolumeCube();
    public function getSurfaceArea();
}
