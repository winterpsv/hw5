<?php

declare(strict_types=1);

namespace tasks\task30;

/**
 * Class Figure
 * @package tasks\task30
 */
abstract class Figure
{
    abstract public function getSquare();
    abstract public function getPerimeter();
    abstract public function getSumSP();
}
