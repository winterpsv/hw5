<?php

declare(strict_types=1);

namespace tasks\task18;

use tasks\task18\Post as Post18;

/**
 * Class Employee
 * @package tasks\task18
 */
class Employee
{
    /** @var string $name */
    private string $name;

    /** @var string $surname */
    private string $surname;

    /** @var Post18 $post */
    private Post18 $post;

    /**
     * Employee constructor.
     * @param string $name
     * @param string $surname
     * @param Post18 $post
     */
    public function __construct(string $name, string $surname, Post18 $post)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->post = $post;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return Post18
     */
    public function getPost(): Post18
    {
        return $this->post;
    }

    /**
     * @param Post18 $post
     */
    public function changePost(Post18 $post)
    {
        $this->post = $post;
    }
}