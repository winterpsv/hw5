<?php

declare(strict_types=1);

namespace tasks\task32;

/**
 * Class Country
 * @package tasks\task32
 */
class Country
{
    use Helper;

    /** @var int $population */
    private int $population;

    /**
     * Country constructor.
     * @param string $name
     * @param int $age
     * @param int $population
     */
    public function __construct(string $name, int $age, int $population)
    {
        $this->name = $name;
        $this->age = $age;
        $this->population = $population;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }
}
