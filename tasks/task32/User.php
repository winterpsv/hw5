<?php

declare(strict_types=1);

namespace tasks\task32;

/**
 * Class User
 * @package tasks\task32
 */
class User
{
    use Helper;

    /**
     * User constructor.
     * @param string $name
     * @param int $age
     */
    public function __construct(string $name, int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }
}
