<?php

declare(strict_types=1);

namespace tasks\task32;

/**
 * Trait Helper
 * @package tasks\task32
 */
trait Helper
{
    /** @var string $name */
    private string $name;

    /** @var int $age */
    private int $age;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}
