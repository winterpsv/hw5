<?php

declare(strict_types=1);

namespace tasks\task24;

/**
 * Class User
 * @package tasks\task24
 */
class User implements UserInterface
{
    /** @var string $name */
    private string $name;

    /** @var int $age */
    private int $age;

    /**
     * @param $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}
