<?php

declare(strict_types=1);

namespace tasks\task24;

/**
 * Interface UserInterface
 * @package tasks\task24
 */
interface UserInterface
{
    public function setName($name); // установить имя
    public function getName(); // получить имя
    public function setAge($age); // установить возраст
    public function getAge(); // получить возраст
}
