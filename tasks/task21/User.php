<?php

declare(strict_types=1);

namespace tasks\task21;

use DateTime;
use Exception;

/**
 * Class User
 * @package tasks\task21
 */
class User
{
    /** @var string $name */
    private string $name;

    /** @var string $surname */
    private string $surname;

    /** @var string $birthday */
    private string $birthday;

    /** @var string $age */
    private string $age;

    /**
     * User constructor.
     * @param string $name
     * @param string $surname
     * @param string $date
     * @throws Exception
     */
    public function __construct(string $name, string $surname, string $date)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->birthday = $date;
        $this->age = $this->calculateAge($date);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getBirthday(): string
    {
        return $this->birthday;
    }

    /**
     * @return string
     */
    public function getAge(): string
    {
        return $this->age;
    }

    /**
     * @param string $birthday
     *
     * @return string
     * @throws Exception
     */
    private function calculateAge(string $birthday): string
    {
        $age = '';

        try {
            $datetime = new DateTime($birthday);
            $interval = $datetime->diff(new DateTime(date("Y-m-d")));
            $age = $interval->format("%Y");
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $age;
    }
}

