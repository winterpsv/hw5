<?php

declare(strict_types=1);

namespace tasks\task19;

use tasks\task3\User as User3;

/**
 * Class Employee
 * @package tasks\task19
 */
class Employee extends User3
{
    /** @var string $surname */
    private string $surname;

    /** @var string $level */
    protected string $level;

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    protected function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    protected function setLevel(string $level): void
    {
        $this->level = $level;
    }

    /**
     * @param string $level
     *
     * @return bool
     */
    protected function validateLevel(string $level): bool
    {
        return in_array($level, array('junior','middle','senior'));
    }
}
