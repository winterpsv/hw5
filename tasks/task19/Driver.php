<?php

declare(strict_types=1);

namespace tasks\task19;

/**
 * Class Driver
 * @package tasks\task19
 */
class Driver extends Employee
{
    /** @var string $experience */
    private string $experience;

    /** @var string $category */
    private string $category;

    /**
     * Driver constructor.
     * @param int $salary
     */
    public function __construct(int $salary)
    {
        (!$this->validateSalary($salary)) ?: $this->setSalary($salary);
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }

    /**
     * @return string
     */
    public function getExperience(): string
    {
        return $this->experience;
    }

    /**
     * @param string $experience
     */
    public function setExperience(string $experience): void
    {
        $this->experience = $experience;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }
}
