<?php

declare(strict_types=1);

namespace tasks\task19;

/**
 * Class Programmer
 * @package tasks\task19
 */
class Programmer extends Employee
{
    /** @var array $langs */
    private array $langs = [];

    public function __construct(string $level)
    {
        (!$this->validateLevel($level))?:$this->setLevel($level);
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @return array
     */
    public function getLangs(): array
    {
        return $this->langs;
    }

    /**
     * @param array $langs
     */
    public function setLangs(array $langs): void
    {
        $this->langs = $langs;
    }
}
