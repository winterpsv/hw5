<?php

declare(strict_types=1);

namespace tasks\task26;

/**
 * Class User
 * @package tasks\task26
 */
class User implements UserInterface
{
    /** @var string $name */
    private string $name;

    /** @var int $age */
    private int $age;

    /**
     * User constructor.
     * @param $name
     * @param $age
     */
    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}
