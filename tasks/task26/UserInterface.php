<?php

declare(strict_types=1);

namespace tasks\task26;

/**
 * Interface UserInterface
 * @package tasks\task26
 */
interface UserInterface
{
    public function __construct($name, $age);
    public function getName();
    public function getAge();
}
