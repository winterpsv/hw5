<?php

declare(strict_types=1);

namespace tasks\task33;

/**
 * Trait Trait2
 * @package tasks\task33
 */
trait Trait2
{
    /**
     * @return int
     */
    private function method2(): int
    {
        return 2;
    }
}
