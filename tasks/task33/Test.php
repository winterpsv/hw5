<?php

declare(strict_types=1);

namespace tasks\task33;

/**
 * Class Test
 * @package tasks\task33
 */
class Test
{
    use Trait1;
    use Trait2;
    use Trait3;

    /**
     * @return int
     */
    public function getSum(): int
    {
        return $this->method1() + $this->method2() + $this->method3();
    }
}
