<?php

declare(strict_types=1);

namespace tasks\task33;

/**
 * Trait Trait3
 * @package tasks\task33
 */
trait Trait3
{
    /**
     * @return int
     */
    private function method3(): int
    {
        return 3;
    }
}
