<?php

declare(strict_types=1);

namespace tasks\task33;

/**
 * Trait Trait1
 * @package tasks\task33
 */
trait Trait1
{
    /**
     * @return int
     */
    private function method1(): int
    {
        return 1;
    }
}
