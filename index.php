<?php

declare(strict_types=1);

define("DELIMITER", "--------------<br>");

require_once __DIR__ . '/vendor/autoload.php';

use tasks\task17\User as User17;
use tasks\task17\Employee as Employee17;
use tasks\task17\City as City17;
use tasks\task18\Post as Post18;
use tasks\task18\Employee as Employee18;
use tasks\task19\Driver as Driver19;
use tasks\task19\Programmer as Programmer19;
use tasks\task20\Employee as Employee20;
use tasks\task21\User as User21;
use tasks\task21\Employee as Employee21;
use tasks\task22\ArraySumHelper as ArraySumHelper22;
use tasks\task23\Num as Num23;
use tasks\task24\User as User24;
use tasks\task25\Cube as Cube25;
use tasks\task26\User as User26;
use tasks\task27\Employee as Employee27;
use tasks\task28\Cube as Cube28;
use tasks\task28\Figure as Figure28;
use tasks\task28\Figure3d as Figure3d28;
use tasks\task28\Quadrate as Quadrate28;
use tasks\task28\Rectangle as Rectangle28;
use tasks\task29\Rectangle as Rectangle29;
use tasks\task29\Disk as Disk29;
use tasks\task33\Test as Test33;
use tasks\task40\ApartmentHouse as ApartmentHouse40;
use tasks\task40\Window as Window40;
use tasks\task40\Door as Door40;

/*
 * Task 17
 *
 * Задача 17.1: Сделайте класс User с публичным свойствами name (имя) и surname (фамилия).
 *
 * Задача 17.2: Сделайте класс Employee, который будет наследовать от класса User и добавлять salary (зарплата).
 *
 * Задача 17.3: Сделайте класс City с публичными свойствами name (название города) и population (количество населения).
 *
 * 	Задача 17.4: Создайте 3 объекта класса User, 3 объекта класса Employee, 3 объекта класса City,
 * и в произвольном порядке запишите их в массив $arr.
 *
 * Задача 17.5: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов,
 * которые принадлежат классу User или потомку этого класса.
 *
 * Задача 17.6: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов,
 * которые НЕ принадлежат классу User или потомку этого класса.
 *
 * Задача 17.7: Переберите циклом массив $arr и выведите на экран столбец свойств name тех объектов,
 * которые принадлежат именно классу User, то есть не классу City и не классу Employee.
 */
$User17Some1 = new User17();
$User17Some1->name = 'Вася';
$User17Some1->surname = 'Пупкин';

$User17Some2 = new User17();
$User17Some2->name = 'Костя';
$User17Some2->surname = 'Петренко';

$User17Some3 = new User17();
$User17Some3->name = 'Андрей';
$User17Some3->surname = 'Васильев';

$Employee17Some1 = new Employee17();
$Employee17Some1->name = 'Коля';
$Employee17Some1->surname = 'Петров';
$Employee17Some1->salary = 2100;

$Employee17Some2 = new Employee17();
$Employee17Some2->name = 'Игорь';
$Employee17Some2->surname = 'Сидоров';
$Employee17Some2->salary = 3400;

$Employee17Some3 = new Employee17();
$Employee17Some3->name = 'Петя';
$Employee17Some3->surname = 'Петров';
$Employee17Some3->salary = 4200;

$City17Some1 = new City17();
$City17Some1->name = 'Харьков';
$City17Some1->population = 3200;

$City17Some2 = new City17();
$City17Some2->name = 'Киев';
$City17Some2->population = 7400;

$City17Some3 = new City17();
$City17Some3->name = 'Львов';
$City17Some3->population = 6800;

$arr = [];
$arr[] = $User17Some1;
$arr[] = $User17Some2;
$arr[] = $User17Some3;

$arr[] = $Employee17Some1;
$arr[] = $Employee17Some2;
$arr[] = $Employee17Some3;

$arr[] = $City17Some1;
$arr[] = $City17Some2;
$arr[] = $City17Some3;

foreach ($arr as $item) {
    if ($item instanceof User17) {
        echo $item->name . '<br>';
    }
}

echo DELIMITER;

foreach ($arr as $item) {
    if ($item instanceof City17) {
        echo $item->name . '<br>';
    }
}

echo DELIMITER;

foreach ($arr as $item) {
    if (($item instanceof User17) && !($item instanceof Employee17)) {
        echo $item->name . '<br>';
    }
}

echo DELIMITER;

/*
 * Task 18
 *
 * Задача 18.1: Сделайте класс Post (должность), в котором будут следующие свойства,
 * доступные только для чтения:name (название должности) и salary (зарплата на этой должности).
 *
 * Задача 18.2: Создайте несколько объектов класса Post: программист, менеджер водитель.
 *
 * Задача 18.3: Сделайте класс Employee (работник),
 * в котором будут следующие свойства: name (имя) и surname (фамилия).
 * Пусть начальные значения этих свойств будут передаваться параметром в конструктор.
 *
 * Задача 18.4: Сделайте геттеры и сеттеры для свойств name и surname.
 *
 * Задача 18.5: Пусть теперь третьим параметром конструктора будет передаваться должность работника,
 * представляющая собой объект класса Post. Укажите тип этого параметра в явном виде.
 *
 * Задача 18.6: Сделайте так, чтобы должность работника (то есть переданный объект с должностью)
 * записывалась в свойство post.
 *
 * Задача 18.7: Создайте объект класса Employee с должностью программист.
 * При его создании используйте один из объектов класса Post, созданный нами ранее.
 *
 * Задача 18.8: Выведите на экран имя, фамилию, должность и зарплату созданного работника.
 *
 * Задача 18.9: Реализуйте в классе Employee метод changePost, который будет изменять должность работника на другую.
 * Метод должен принимать параметром объект класса Post. Укажите в методе тип принимаемого параметра в явном виде.
 */

$Post18Some1 = new Post18('Программист', 2500);
$Post18Some2 = new Post18('Менеджер', 1500);
$Post18Some3 = new Post18('Водитель', 800);

$Employee18Some1 = new Employee18('Вася', 'Пупкин', $Post18Some1);
echo $Employee18Some1->getName() . '<br>';
echo $Employee18Some1->getSurname() . '<br>';
echo $Employee18Some1->getPost()->getName() . '<br>';
echo $Employee18Some1->getPost()->getSalary() . '<br>';

echo DELIMITER;

$Employee18Some1->changePost($Post18Some3);
echo $Employee18Some1->getName() . '<br>';
echo $Employee18Some1->getSurname() . '<br>';
echo $Employee18Some1->getPost()->getName() . '<br>';
echo $Employee18Some1->getPost()->getSalary() . '<br>';

echo DELIMITER;

/*
 * Task 19
 *
 * Задача 19.1: Есть класс User из задачи 3.
 *
 * Задача 19.2: Сделайте класс Employee,
 * который будет наследовать от класса User и расширьте его 1 новым свойством и 1 новым методом.
 *
 * Задача 19.3: Сделайте класс Programmer, который будет наследовать от класса Employee.
 * Пусть новый класс имеет свойство langs, в котором будет хранится массив языков,
 * которыми владеет программист. Сделайте также геттер и сеттер для этого свойства.
 *
 * Задача 19.4: Сделайте класс Driver (водитель), который будет наследовать от класса Employee.
 * Пусть новый класс добавляет следующие свойства: водительский стаж, категория вождения (A, B, C, D),
 * а также геттеры и сеттеры к ним.
 *
 * Задача 19.5: Добавьте в класс User 1 свойство и 2 метода для него,
 * которые нельзя будет использовать снаружи класса, но можно будет использовать в классе  Driver.
 * Используйте это свойство и методы.
 *
 * Задача 19.6: Сделайте аналогичные действия (как в задаче 19.5),
 * только для класса Employee и Programmer соответственно.
 */
$Driver19Some1 = new Driver19(800);
echo $Driver19Some1->getSalary() . '<br>';

echo DELIMITER;

$Programmer19Some1 = new Programmer19('middle');
echo $Programmer19Some1->getLevel() . '<br>';

echo DELIMITER;

/*
 * Task 20
 *
 * Задача 20.1: Есть класс Student из задачи 8.
 * Если по ходу выполнения данного задания понадобятся новые свойства - создайте их.
 *
 * Задача 20.2: Добавьте в этот класс валидатор для проверки имени и фамилии курс-координатора,
 * сделайте так, чтобы его можно было использовать в классах наследниках.
 *
 * Задача 20.3: Создайте новый класс (на ваше усмотрение), который будет наследоваться от класса Student.
 *
 * Задача 20.4: Переопределите родительский метод валидатор, чтобы он проверял еще и отчество курс-координатора,
 * помимо родительской проверки.
 */
$Employee20Some1 = new Employee20();
$Employee20Some1->setCourseAdministrator('Вася');
$Employee20Some1->setCourseAdministratorSurname('Пупкин');
$Employee20Some1->setCourseAdministratorPatronymic('Александрович');
echo $Employee20Some1->validateCourseAdministrator() . '<br>';

echo DELIMITER;
/*
 * Task 21
 *
 * Задача 21.1: Сделайте класс User, в котором будут следующие свойства только для чтения:
 * name (имя), surname (фамилия), Начальные значения этих свойств должны устанавливаться в конструкторе.
 * Сделайте также геттеры этих свойств.
 *
 * Задача 21.2: Сделайте так, чтобы третьим параметром в конструктор передавалась дата рождения работника в
 * формате год-месяц-день. Запишите ее в свойство birthday. Сделайте геттер для этого свойства.
 *
 * Задача 21.3: Сделайте приватный метод calculateAge, который параметром будет принимать дату рождения,
 * а возвращать возраст с учетом того, был ли уже день рождения в этом году, или нет.
 *
 * Задача 21.4: Сделайте так, чтобы метод calculateAge вызывался в конструкторе объекта,
 * рассчитывал возраст пользователя и записывал его в приватное свойство age. Сделайте геттер для этого свойства.
 *
 * Задача 21.5: Сделайте класс Employee, который будет наследовать от класса User.
 * Пусть новый класс имеет свойство salary, в котором будет хранится зарплата работника.
 * Зарплата должна передаваться четвертым параметром в конструктор объекта.
 * Сделайте также геттер для этого свойства.
 */
$User21Some1 = new User21('Serega', 'Pedchenko', '1988-03-25');
echo $User21Some1->getAge() . '<br>';

echo DELIMITER;

$Employee21Some2 = new Employee21('Коля', 'Попкин', '1983-06-14', 3800);
echo $Employee21Some2->getAge() . '<br>';
echo $Employee21Some2->getSalary() . '<br>';

echo DELIMITER;

/*
 * Task 22
 *
 * Задача 22.1: Сделайте класс ArraySumHelper.
 *
 * Задача 22.2: Добавьте в него метод, который будет принимать первым аргументом массив чисел,
 * а вторым - число, во сколько раз нужно умножить каждый из элементов массива.
 * Метод должен возвращать итоговый массив с умноженными элементами.
 *
 * Задача 22.3: Пусть дан массив с числами. Найдите с помощью класса
 * ArraySumHelper сумму квадратов элементов этого массива.
 *
 * Задача 22.4: Переделайте класс ArraySumHelper на работу с статикой.
 */
$mus = array(1, 3, 4, 33, 77, 32);
$ArraySumHelper22Some1 = ArraySumHelper22::arrayElementMultiplier($mus, 3);

print_r($ArraySumHelper22Some1);

echo '<br>' . DELIMITER;

/*
 * Task 23
 *
 * Задача 23.1: Сделайте класс Num, у которого будут два публичных статических свойства: num1 и num2.
 * Запишите в первое свойство число 2, а во второе - число 3. Выведите сумму значений свойств на экран.
 *
 * Задача 23.2: Добавьте в класс Num, два приватны статических свойства: num3 и num4.
 * Пусть по умолчанию в свойстве num3 хранится число 3, а в свойстве num4 - число 5.
 *
 * Задача 23.3: Добавьте в класс Num метод getSum, который будет выводить на экран сумму значений свойств num3 и num1.
 *
 * Задача 23.4: Добавьте в класс Num две константы
 * (названия и значения придумайте сами исходя из логики использования констант).
 * Выведите на экран значение первой константы напрямую, и значение второй константы через геттер.
 */
Num23::$num1 = 2;
Num23::$num2 = 3;

echo Num23::$num1 + Num23::$num2 . '<br>';

echo Num23::getSum() . '<br>';

echo Num23::NUM5 . '<br>';

echo Num23::getNum6() . '<br>';

echo DELIMITER;

/*
 * Task 24
 *
 * Задача 24.1:: Пусть у нас дан такой интерфейс UserInterface -
 * https://gist.github.com/aksafan/973157d6ddb36dd24e3e901b7e9a5fcf
 *
 * Задача 24.2: Сделайте класс User, который будет реализовывать данный интерфейс.
 */
$User24Some1 = new User24();
$User24Some1->setName('Serega');
$User24Some1->setAge(31);

echo $User24Some1->getName() . '<br>';
echo $User24Some1->getAge() . '<br>';

echo DELIMITER;

/*
 * Task 25
 *
 * Задача 25.1: Сделайте интерфейс CubeInterface, который будет описывать фигуру Куб.
 *
 * Задача 25.2: Пусть ваш интерфейс описывает конструктор, параметром принимающий сторону куба,
 * а также методы для получения объема куба и площади поверхности.
 *
 * Задача 25.3: Сделайте класс Cube, реализующий интерфейс CubeInterface.
 */
$Cube25Some1 = new Cube25(5);

echo $Cube25Some1->getVolumeCube() . '<br>';
echo $Cube25Some1->getSurfaceArea() . '<br>';

echo DELIMITER;

/*
 * Task 26
 *
 * Задача 26.1: Сделайте интерфейс UserInterface, который будет описывать юзера.
 *
 * Задача 26.2: Предполагается, что у юзера будет имя и возраст и эти поля будут передаваться параметрами конструктора.
 *
 * Задача 26.3: Пусть ваш интерфейс также задает то, что у юзера будут геттеры (но не сеттеры) для имени и возраста.
 *
 * 	Задача 26.4: Сделайте класс User, реализующий интерфейс UserInterface.
 */
$User26Some1 = new User26('Serega', 31);

echo $User26Some1->getName() . '<br>';
echo $User26Some1->getAge() . '<br>';

echo DELIMITER;

/*
 * Task 27
 *
 * Задача 27.1: Сделайте интерфейс UserInterface с методами getName, setName, getAge, setAge.
 *
 * Задача 27.2: Сделайте интерфейс EmployeeInterface, наследующий от интерфейса UserInterface
 * и добавляющий в него методы getSalary и setSalary.
 *
 * Задача 27.3: Сделайте класс Employee, реализующий интерфейс EmployeeInterface.
 */
$Employee27Some1 = new Employee27();
$Employee27Some1->setName('Вася');
$Employee27Some1->setAge(33);
$Employee27Some1->setSalary(3200);

echo $Employee27Some1->getName() . '<br>';
echo $Employee27Some1->getAge() . '<br>';
echo $Employee27Some1->getSalary() . '<br>';

echo DELIMITER;

/*
 * Task 28
 *
 * Задача 28.1: Сделайте интерфейс Figure (простая фигура), который будет иметь методы getSquare и getPerimeter.
 *
 * Задача 28.2: Сделайте интерфейс Figure3d (трехмерная фигура),
 * который будет иметь метод getVolume (получить объем) и метод getSurfaceSquare (получить площадь поверхности).
 *
 * Задача 28.3: Сделайте класс Cube (куб), который будет реализовывать интерфейс Figure3d.
 *
 * Задача 28.4: Создайте несколько объектов класса Quadrate,
 * несколько объектов класса Rectangle и несколько объектов класса Cube (внимательно продумайте используемые интерфейсы).
 * Запишите их в массив $arr в случайном порядке.
 *
 * Задача 28.5: Переберите циклом массив $arr и выведите на экран только площади объектов реализующих интерфейс Figure.
 *
 * Задача 28.6: Переберите циклом массив $arr и выведите для плоских фигур их площади,
 * а для объемных - площади их поверхности.
 */
unset($arr);
$arr = [];
$Cube28Some1 = new Cube28(6);
$arr[] = $Cube28Some1;
$Cube28Some2 = new Cube28(9);
$arr[] = $Cube28Some2;
$Cube28Some3 = new Cube28(15);
$arr[] = $Cube28Some3;

$Quadrate28Some1 = new Quadrate28(8);
$arr[] = $Quadrate28Some1;
$Quadrate28Some2 = new Quadrate28(12);
$arr[] = $Quadrate28Some2;
$Quadrate28Some3 = new Quadrate28(22);
$arr[] = $Quadrate28Some3;

$Rectangle28Some1 = new Rectangle28(4, 3);
$arr[] = $Rectangle28Some1;
$Rectangle28Some2 = new Rectangle28(32, 45);
$arr[] = $Rectangle28Some2;
$Rectangle28Some3 = new Rectangle28(65, 25);
$arr[] = $Rectangle28Some3;

foreach ($arr as $item) {
    if ($item instanceof Figure28) {
        echo 'Figure28 - ' . $item->getSquare() . '<br>';
    }
}

echo DELIMITER;

foreach ($arr as $item) {
    if ($item instanceof Figure28) {
        echo 'Figure28 - ' . $item->getSquare() . '<br>';
    } elseif ($item instanceof Figure3d28) {
        echo 'Figure3d28 - ' . $item->getSurfaceSquare() . '<br>';
    }
}

echo DELIMITER;

/*
 * Task 29
 *
 * Задача 29.1: У нас есть интерфейсы из задачи 28, плюс сделайте интерфейс Tetragon (четырехугольник),
 * у которого будут гетеры для для всех четырех сторон четырехугольника.
 *
 * Задача 29.2: Сделайте так, чтобы класс Rectangle реализовывал два интерфейса: и Figure, и Tetragon.
 *
 * Задача 29.1: Сделайте интерфейс Circle (круг) с методами getRadius (получить радиус) и
 * getDiameter (получить диаметр).
 *
 * Задача 29.3: Сделайте так, чтобы класс Disk реализовывал два интерфейса: и Figure, и Circle.
 */
$Rectangle29Some1 = new Rectangle29(6, 5, 6, 5);

echo $Rectangle29Some1->getSquare() . '<br>';

$Disk29Some1 = new Disk29(8);

echo $Disk29Some1->getPerimeter() . '<br>';

echo DELIMITER;

/*
 * Task 30
 *
 * Задача 30.1: Дано: нам необходимо работать с геометрическими фигурами, например,
 * квадратами, прямоугольниками, треугольниками и так далее. Пусть каждая фигура будет описываться своим классом,
 * при этом мы хотим сделать так, чтобы каждый класс имел метод для вычисления площади и метод для вычисления
 * периметра фигуры, также, метод для нахождения суммы площади и периметра.
 *
 * Задача 30.2: Придумайте и реализуйте архитектуру для данной логики через абстрактный класс и классы
 * конкретных фигур. Помните, что у каждой фигуры есть свои особенности, их нужно учитывать.
 */

/*
 * Task 31
 *
 * Задача 31.1: Реализуйте архитектуру из задачи 30, теперь используя интерфейсы.
 *
 * Задача 31.2: Сделайте класс FiguresCollection, который будет хранить в себе массив объектов-фигур.
 *
 * Задача 31.3: Сделайте в классе FiguresCollection метод addFigure	для
 * добавления объектов в коллекцию (в массив свойства класса).
 * озаботьтесь о том, чтобы данный метод 100% принимал аргументом именно какую-то из фигур (любую).
 *
 * Задача 31.4: Сделайте в классе FiguresCollection метод getTotalSquare,
 * находящий полную площадь фигур всей коллекции (всего массива).
 * Подсказка: у каждой фигуры должен быть способ найти площадь.
 */

/*
 * Task 32
 *
 * Задача 32.1: Реализуйте трейт Helper, содержащий приватные свойства name и age, а также их геттеры.
 *
 * Задача 32.2: Реализуйте класс Country (страна) со свойствами name (название), age (возраст),
 * population (количество населения) и геттерами (по надобности) для них.
 *
 * Задача 32.3: Реализуйте класс User, в конструкторе которого задаются свойства
 * name и age по аналогии с задачей 32.2.
 *
 * Задача 32.4: Пусть наш классы для сокращения своего кода используют, уже созданный в задаче 32.1, трейт Helper.
 * При надобности, отрефакторите код наших классов.
 */

/*
 * Task 33
 *
 * Задача 33.1: Сделайте 3 трейта с названиями: Trait1, Trait2 и Trait3.
 * Пусть в первом трейте будет метод method1, возвращающий 1, во втором трейте - метод method2, возвращающий 2,
 * а в третьем трейте - метод method3, возвращающий 3.Пусть все эти методы будут приватными.
 *
 * Задача 33.2: Сделайте класс Test, использующий все три созданных нами трейта.
 * Сделайте в этом классе публичный метод getSum, возвращающий сумму результатов методов подключенных трейтов.
 */
$Test33Some1 = new Test33();

echo $Test33Some1->getSum() . '<br>';

echo DELIMITER;

/*
 * Task 40
 *
 * Задача 40.1: Есть класс Building (здание). Нам нужно создать класс PrivateHouse (частный дом),
 * класс ApartmentHouse (многоквартирный дом), класс Dugout (землянка), класс Fridge (холодильник),
 * класс Wardrobe (шкаф), класс Microwave (микроволновка).
 *
 * Задача 40.2: У всех этих классов есть некоторые схожие элементы и функционал (к примеру, двери/окна,
 * которые могут открываться/закрываться). Ваша задача создать все эти классы таким образом,
 * чтобы уменьшить до минимума повторение кода и оптимизировать его использование.
 * Помните о различиях в является и имеет.
 */
$ApartmentHouse40Some1 = new ApartmentHouse40('ApartmentHouse');
$ApartmentHouse40Some1->addSpecifications(new Window40(array('one' => 'two'), array('width' => 123, 'height' => 204)));
print_r($ApartmentHouse40Some1->getWindow('open'));

echo '<br>' . DELIMITER;

/*
 * Task 45
 *
 * Задача 45.1: У каждого из вас есть библиотека, написанная по второй домашке.
 * Ваша задача, установить себе в проект все библиотеки каждого студента группы и
 * использовать любой функционал с каждой библиотеки.
 *
 * Задача 45.2: То есть, результатом выполнения этого задания будет:
 * 13 установленных через композер библиотек;
 * 13 вызванных методов, по одной из каждой библиотеки (можно больше, главное, чтобы минимум по одному методу с каждой);
 * полностью рабочий функционал каждого вызова каждого метода.
 */

use winterpsv\winter\Calc;

$square = new Calc();
$square->counts('Rectangle', 3, 8);
$square->getResult();

echo DELIMITER;

use Nechiporenko\Classes\App;

$app = new App();

$app->execute(12, 3);
$app->execute(5, 2);
$app->showTotal();

echo DELIMITER;

use classPension\pensionFond;

$pensionAge = 65;

$employees = [];

$employees[] = new pensionFond();
$employees[0]->name = 'Vasya';
$employees[0]->age = 36;
$employees[0]->city = 'Kyiv';

$employees[] = new pensionFond();
$employees[1]->name = 'Petya';
$employees[1]->age = 41;
$employees[1]->city = 'Kyiv';

$employees[] = new pensionFond();
$employees[2]->name = 'Misha';
$employees[2]->age = 23;


//Добавление функции is_countable()
if (is_countable($employees)) {
    // Добавление функций array_key_first() и array_key_last()
    for ($i = array_key_first($employees); $i <= array_key_last($employees); $i++) {
        echo "<b>" . $employees[$i]->name . " :</b><br>";
        echo "Живет в : ";
        $employees[$i]->showCity();
        echo "Осталось до пенсии : " . $employees[$i]->yearsToPension($pensionAge) . ' лет <br><br>';
    }
} else {
    echo 'Error while filling the array!';
}

echo DELIMITER;

use devswb\hometask2\discriminantCalc\DiscriminantCalc;

$DiscriminantCalc = new DiscriminantCalc(1,2,3);

echo $DiscriminantCalc->discriminant();
$DiscriminantCalc->Result();

echo DELIMITER;

$fiche = new \classes\ShowFiche('is_countable');
echo $fiche->showFiche();

echo DELIMITER;

$Website = new webSite\Website();
/* Variables are needed for testing methods.
 * Do not delete!!
 * #################
 */
$arr_roles = ['admin' => '1', 'user' => '2', 'manager' => '3'];
$url_comment = 'http://test.site/post-1/?get=name&comment-220';
/*
 * ################
 */
// TEST method
echo $Website->get_last_role( $arr_roles );

echo DELIMITER;

use lysenkolipa\calcLiba\basicCalculator\BasicCalculator;
use lysenkolipa\calcLiba\fuelCalculator\FuelCalculator;

$calcBase = new BasicCalculator(5.3,4.0);
$calcBase->getMultiplication();
$calcBase->getSubtraction();
$calcBase->getSumm();
$calcBase->displayResult();

$fuelCost = new FuelCalculator(234.1, 9.0, 33);
$fuelCost->getFuelCostPerTrip();
$fuelCost->displayFuelCostPerTrip();

echo DELIMITER;

use classes\Argon2;
use classes\Bar;
use classes\Calculate;
use classes\CoalescingOperator;
use classes\Employee;
use classes\IsCountable;
use classes\KeyArray;
use classes\Token;
//1
$emp = new Employee(4);
echo var_dump($emp->getId());
//2
$calc = new Calculate();
var_dump($calc->getSum(1, 2));
//3
$tok = new Token();

//4
$argon2 = new Argon2();
$argon2->setPass("abra");
$argon2->hashPasswordArgon();
echo $argon2->pass;

echo DELIMITER;

use psydo\packagisttest\dumbclass\Test1;

$test1 = new Test1();
echo $test1;

echo DELIMITER;

use pelukho\userfaker\src\UserFaker;

$fakeUser = new UserFaker("m", "de");
echo $fakeUser->getUserFullName();

echo DELIMITER;

use Platon\Platon;

$pay = new Platon('TEST000001', 'P@$$vv0r|)');

echo $pay->getHtmlForm([
                           'payment'     => 'CC',
                           'amount'      => '1.99',
                           'currency'    => 'UAH',
                           'description' => 'Test description',
                           'url'         => 'http://exmple.com',
                       ]);

echo DELIMITER;
